#!/usr/bin/env python
# coding: utf-8

import pickle
from sf_vehthft_helperfuns import *


def get_risk(sample):
    # extract time related features for model
    hour = get_hour(sample['Time'])
    dayoweek = get_dow(sample['Time'])
    fdayoweek = get_fdow(sample['Time'])

    # get coordinates from json and convert to df for feeding to model
    df = pd.DataFrame(sample, index=[0])
    df = df[['Latitude','Longitude']]
  
    # load the label encoders from disk
    pdistrict_labenc = pickle.load(open('pdistrict_labenc.pkl', 'rb'))
    sdistrict_labenc = pickle.load(open('sdistrict_labenc.pkl', 'rb'))
    hood_labenc = pickle.load(open('hood_labenc.pkl', 'rb'))
    inter_labenc = pickle.load(open('inter_labenc.pkl', 'rb'))

    # load the preprocessing models from disk
    pdistrict_knn = pickle.load(open('pdistrict_knn.pkl', 'rb'))
    sdistrict_lgbm = pickle.load(open('sdistrict_lgbm.pkl', 'rb'))
    hood_knn = pickle.load(open('hood_knn.pkl', 'rb'))
    inter_knn = pickle.load(open('inter_knn.pkl', 'rb'))

    # get preprocess predictions
    pdresult = pdistrict_knn.predict(df)
    sdresult = sdistrict_lgbm.predict(df)
    hresult = hood_knn.predict(df)
    iresult = inter_knn.predict(df)

    # convert predictions to preprocessed labels
    pdistrict = pdistrict_labenc.inverse_transform(pdresult)
    sdistrict = sdistrict_labenc.inverse_transform(sdresult)
    hood = hood_labenc.inverse_transform(sdresult)
    inter = inter_labenc.inverse_transform(iresult)

    # get mappings from dics in helper_funs
    isection = inter[0]
    isection_te = inter_te.get(isection)
    nvt_te = NVT_TE.get(isection)
    burg_te = burglary_te.get(isection)

    # convert all preprocess features to df
    df = pd.DataFrame({'Incident_Hour': hour,
                 'Incident_Day_of_Week': dayoweek,
                 'Police_District': pdistrict,
                 'Analysis_Neighborhood': hood,
                 'Supervisor_District': sdistrict,
                 'Inter_TE': isection_te,
                 'NVT_TE': nvt_te,
                 'Burglary_TE': burg_te})

    # one hot encode the categorical features
    df2 = pd.get_dummies(df[variables], columns=catvars)

    # add in columns to complete structure of df for final model
    for col in features:  
        if col not in list(df2):
            df2[col] = 0

    df2 = df2[features]

    # score the df row
    vthft_model = pickle.load(open('vthft_model.pkl', 'rb'))
    prob = vthft_model.predict_proba(df2)[:,1]

    # classify the risk score into risk levels based on quartile cutoff vals
    if prob > 0.30435894561204613:
        x = 'Very High'
    elif prob > 0.16207417995553636 and prob < 0.30435894561204613:
        x = 'High'
    elif prob > 0.07504314462280023 and prob < 0.16207417995553636:
        x = 'Medium'
    else:
        x = 'Low'

     # convert all preprocess features to df for 3 hour advance risk score
    hour2 = hour + 3
    
    if hour2 > 23:
        newhour = hour2 - 24
    else:
        newhour = hour2
    
    if hour2 > 23:
        day = fdayoweek
    else:
        day = dayoweek
    
    df3 = pd.DataFrame({'Incident_Hour': newhour,
                 'Incident_Day_of_Week': day,
                 'Police_District': pdistrict,
                 'Analysis_Neighborhood': hood,
                 'Supervisor_District': sdistrict,
                 'Inter_TE': isection_te,
                 'NVT_TE': nvt_te,
                 'Burglary_TE': burg_te})

    # one hot encode the categorical features
    df4 = pd.get_dummies(df3[variables], columns=catvars)

    # add in columns to complete structure of df for final model
    for col in features:  
        if col not in list(df4):
            df4[col] = 0

    df4 = df4[features]

    # score the df row
    prob2 = vthft_model.predict_proba(df4)[:,1]

    # classify the risk score into risk levels based on quartile cutoff vals
    if prob2 > 0.30435894561204613:
        x2 = 'Very High'
    elif prob2 > 0.16207417995553636 and prob2 < 0.30435894561204613:
        x2 = 'High'
    elif prob2 > 0.07504314462280023 and prob2 < 0.16207417995553636:
        x2 = 'Medium'
    else:
        x2 = 'Low'
        
    reponse = {'Risk Score': prob[0], 
               'Risk Level': x,
#                'Hour': hour,
#                'DOW': dayoweek,
               '3+ Hour Risk Score': prob2[0],
               '3+ Hour Risk Level': x2,
#                'FHour': newhour,
#                'FDOW': day
              }
    return reponse

